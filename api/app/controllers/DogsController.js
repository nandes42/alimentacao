const fs = require('fs')
const { DogDao, DogFactory }= require('../models/Dog');
const { LoginHelper, ListHelper } = require('../helpers/index');

class DogsController{

    constructor(){
        this._dogDao = new DogDao();
    }

    async getAll(req, res){
        const login = await LoginHelper.verifyLogin(req, res);
        if(!login.error){
            try {
                const ownerId = login.data.user.id;
                let dogs =  await this._dogDao.findAll({ ownerId });
                dogs = ListHelper.createListCheckingActive(dogs, DogFactory.createFromMongo);
                res.status(200).send(dogs); 
            }catch(error){
                console.log(error);
                res.status(500).send({ error: "Não foi possível carregar os cachorros."})   
            }
        }
    }

    async create(req, res){
        const login = await LoginHelper.verifyLogin(req, res);  
        if(!login.error){
            try {
                const dog = DogFactory.createDogFromReqBody(req.body, login.data.user.id);
                const result = await this._dogDao.insert(dog.jsonToInsert());
                const dogToRes = DogFactory.createFromMongo(result);
                res.status(201).send(dogToRes.json());
            }catch(error){
                console.log(error);
                res.status(500).send({ error: "Não foi possível inserir o cachorro."});   
            }
        } 
    }

    async insertImg(req, res){
        try {
            await LoginHelper.verifyLogin(req, res);
            req.pipe(fs.createWriteStream(`img/${req.headers.filename}.jpg`));
            res.status(200).send({ msg: "Imagem inserida com sucesso." });
        }catch(error){
            res.status(500).send({ error: "Não foi possível inserir a foto."}); 
        }
    }

    async remove(req, res){
        const login = await LoginHelper.verifyLogin(req, res);
        if(!login.error){
            try {
                const result = await this._dogDao.disable(req.params.id);
                res.status(200).send(result);
            }catch(error){
                res.status(500).send({ error: "Não foi possível remover o cachorro."});
            }
        }
    }


}

module.exports = DogsController;