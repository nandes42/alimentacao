const { UserDao, UserFactory }  = require('../models/User');
const { PasswordHelper, JwtHelper, LoginHelper } = require('../helpers');
class UsersController{

    constructor(){
        this._userDao = new UserDao();
        this._passwordHelper = new PasswordHelper();
    }

    async create(req, res){
        try {
            const user = await UserFactory.createFromReqBody(req.body);
            const result = await this._userDao.insert(user.jsonToInsert());
            const userResp = UserFactory.createFromMongo(result);
            res.status(201).send(userResp.json());
        }catch(error){
            res.status(500).send({ error: "Não foi possível criar o usuário." })
        }
    }

    async login(req, res){
        try {
            const result = await this._userDao.findOne({ email: req.body.email });
            if(result){
                const user = UserFactory.createFromMongo(result);
                const userIsValid = await this._passwordHelper.verifyPassword(req.body.password, user.password);
                if(userIsValid) {
                    const jwt = await JwtHelper.createJwt('user', user.json());
                    res.status(201).send({ jwt, user: user.json() });
                    return;
                }
            res.status(401).send({ error: "Não foi possível efetuar o login."});
            }
        }catch(error){
            res.status(500).send({ error: "Não foi possível efetuar login." });
        }
    }

}

module.exports = UsersController;