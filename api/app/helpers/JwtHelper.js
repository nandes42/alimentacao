const jwt = require('jsonwebtoken');
const secret = 'secret'
class JwtHelper {

    static createJwt(key, data){
        const json = {};
        json[key] = data;
        return new Promise((resolve, reject) => {
            jwt.sign(json, secret, (error, token) => {
                if(error) return reject(error);
                return resolve(token);
            });
        });
    }

    static verifyJwt(token){
        return new Promise((resolve, reject) => {
            jwt.verify(token, secret, (error, decoded) => {
                if(error) return reject(error);
                return resolve(decoded);
            })
        });
    }
}

module.exports = JwtHelper;