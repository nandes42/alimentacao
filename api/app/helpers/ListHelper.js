class ListHelper{
    
    static createList(mongoArray, createMethod){
        return mongoArray.map(json => {
            const obj = createMethod(json);
            return obj.json();
        });
    }

    static createListCheckingActive(mongoArray, createMethod){
        const filteredArray = mongoArray.filter(json => json.active);
        return filteredArray.map(json => {
            const obj = createMethod(json);
            return obj.json();
        })
    }
}

module.exports = ListHelper;