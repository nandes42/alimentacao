const JwtHelper = require('./JwtHelper');

class LoginHelper {

    static async verifyLogin(req, res){
        let result = {};
        try{
            const decoded = await JwtHelper.verifyJwt(req.headers.auth);
            result.data = decoded;
            result.error = false;
        }catch(error){
            res.status(401).send({ error: "Efetue o login antes de prosseguir." });
            result.error = true;
        }
        return result; 
    }

}

module.exports = LoginHelper