const bcrypt = require('bcrypt');

class PasswordHelper{
    
    constructor(){
        this._saltRounds = 10;
    }

    encryptPassword(passwd){
        return new Promise((resolve, reject) => {
            bcrypt.hash(passwd, this._saltRounds, (err, hash) =>{
                if(err) return reject(err);
                return resolve(hash);
            })
        });
    }

    verifyPassword(passwd, hash){
        return new Promise((resolve, reject) => {
            bcrypt.compare(passwd, hash, (err, result) => {
                if(err) return reject(err);
                return resolve(result);
            });
        });
    }

    

}

module.exports = PasswordHelper;