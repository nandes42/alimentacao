class Dog{

    constructor(name, breed, ownerId){
        this._ownerId = ownerId;
        this._name = name;
        this._breed = breed;
    }

    get id(){
        return this._id;
    }

    set id(id){
        this._id = id;
    }

    jsonToInsert(){
        return {
            name: this._name,
            breed: this._breed,
            ownerId: this._ownerId
        }
    }

    json(){
        return {
            id: this._id,
            name: this._name,
            breed: this._breed
        }
    }
}

module.exports = Dog;