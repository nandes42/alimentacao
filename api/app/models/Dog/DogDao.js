const { Dao } = require('../../src/index');

class DogDao extends Dao{

    constructor(){
        super('dogs',{
            ownerId: String,
            name: String,
            breed: String,
            active: { type: Boolean, default: true }            
        })
    }
}

module.exports = DogDao;