const Dog = require('./Dog');

class DogFactory {

    static createDogFromReqBody(json, ownerId){
        const dog = new Dog(
            json.name,
            json.breed,
            ownerId
        );
        return dog;
    }

    static createFromMongo(json){
        const dog = new Dog(
            json.name,
            json.breed,
            json.ownerId
        ); 
        dog.id = json._id;
        return dog;
    }
}

module.exports = DogFactory;