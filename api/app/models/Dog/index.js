module.exports = {
    Dog: require('./Dog'),
    DogDao: require('./DogDao'),
    DogFactory: require('./DogFactory')
}