
class User{

    constructor(name, email, password){
        this._name = name;
        this._email = email;
        this._password = password;
    }

    set id(id){
        this._id = id;
    }

    get id(){
        return this._id;
    }

    get password(){
        return this._password;
    }

    jsonToInsert(){
        return {
            name: this._name,
            email: this._email,
            password: this._password
        }
    }

    json(){
        return {
            id: this._id,
            name: this._name,
            email: this._email
        }

    }
}

module.exports = User;