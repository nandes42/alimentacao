const { Dao } = require('../../src/index');

class UserDao extends Dao{

    constructor(){
        super('users', {
            name: String,
            email: String,
            password: String
        })
    }
}

module.exports = UserDao;