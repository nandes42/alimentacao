const User = require('./User');
const { PasswordHelper } = require('../../helpers/index.js');
const pwh = new PasswordHelper();

class UserFactory{

    static createFromReqBody(json){
        return new Promise((resolve, reject) => {
            const name = json.name;
            const email = json.email;
            pwh.encryptPassword(json.password)
                .then(encryptedPassword => new User(name, email, encryptedPassword))
                .then(user => resolve(user))
                .catch(error => reject(error));
        })  
    }

    static createFromMongo(json){
        const user = new User(json.name, json.email, json.password);
        user.id = json._id;
        return user;
    }
    
}

module.exports = UserFactory;