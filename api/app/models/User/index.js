module.exports = {
    User: require('./User'),
    UserDao: require('./UserDao'),
    UserFactory: require('./UserFactory')
}