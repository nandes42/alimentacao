const DogsController = require('../controllers/DogsController');
const dogsCtl = new DogsController();

module.exports = app => {

    app.get(
        '/dogs',
        (req, res) => dogsCtl.getAll(req, res)
        );
         
    app.post(
        '/dogs',
        (req, res) => dogsCtl.create(req, res)
        );
    
    app.delete(
        '/dogs/:id',
        (req, res) => dogsCtl.remove(req, res)
        )
                 
    app.put(
        '/dogs/:id',
        (req, res) => dogsCtl.update(req, res)
        )
    
    app.post(
        '/dogs/image',
        (req, res) => dogsCtl.insertImg(req, res)
    )

}