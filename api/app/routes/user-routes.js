const UsersController = require('../controllers/UsersController');
const userCtl = new UsersController();
const url = '/users';

module.exports = app => {

    app.post(
        url,
        (req, res) => userCtl.create(req, res)
    );

    app.post(
        '/login',
        (req, res) => userCtl.login(req, res)
    );


}