const mongoose = require('mongoose');

class Connction{

    constructor(){
        return mongoose.createConnection('mongodb://mongodb/dog-feeder', { useNewUrlParser: true});
    }
}

module.exports = Connction;