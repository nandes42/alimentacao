const mongoose = require('mongoose');
const Connection = require('./Connection');

class Dao {

    constructor(collection, schema){
        this._collection = collection;
        this._schema = mongoose.Schema(schema); 
    }

    get collection(){
        return this._collection;
    }

    get schema(){
        return this._schema;
    }

    async findAll(filter={}){
        try {
            const connection = new Connection();
            const Model = connection.model(this._collection, this._schema);
            const result = await Model.find(filter);
            connection.close();
            return result;
        }catch(error){
            throw error;
        }
    }

    async findOne(filter){
        try {
            const connection = new Connection();
            const Model = connection.model(this._collection, this._schema);
            const result = await Model.find(filter);
            connection.close();
            return result[0];
        }catch(error){  
            throw error;
        }
    }

    async insert(data){
        try{
            const connection = new Connection(); 
            const Model = connection.model(this._collection, this._schema);
            const model = new Model(data);
            const result = await model.save();
            connection.close();
            return result;
        }catch(error){
            throw error;
        }
    }

    async disable(id){
        try {
            const connection = new Connection();
            const Model = connection.model(this._collection, this._schema);
            const result = await Model.updateOne({ _id: id }, {$set: { active: false }});
            connection.close();
            return result;
        }catch(error){
            throw error;
        }
    }
}

module.exports = Dao;