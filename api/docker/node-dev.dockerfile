FROM node:10
WORKDIR /usr/app    
COPY . .
RUN npm install
EXPOSE 3000
EXPOSE 9229
ENTRYPOINT npm run dev