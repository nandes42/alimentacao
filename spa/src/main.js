import Vue from 'vue'
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import App from './App.vue'
import axios from 'axios';
import { routes } from './routes';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
''
Vue.config.productionTip = false

Vue.use(Vuex);

axios.defaults.baseURL = 'http://localhost:3000';

Vue.use(VueRouter);
const router = new VueRouter({
  mode: 'history',
  routes
});

new Vue({
  router,  
  render: h => h(App),
}).$mount('#app')
