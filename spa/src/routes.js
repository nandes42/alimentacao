import Login from './components/account/Login';
import CreateAccount from './components/account/CreateAccount';
import DogsDashboard from './components/dogs/DogsDashboard';

export const routes =
[
    {
        name: 'create-account',
        path: '/create-account',
        component: CreateAccount
    },
    {
        name: 'login',
        path: '/login',
        component: Login
    },
    {
        name: 'dogs-dashboard',
        path: '/dogs-dashboard',
        component: DogsDashboard
    }
]