import axios from 'axios';

export default class LoginService {

    async create(data){
        try {
            const res =  await axios.post('/login', data);
            localStorage.setItem('jwt', res.data.jwt) 
            localStorage.setItem('user', JSON.stringify(res.data.user))
        }catch(error){
            console.log(error);
        }

    }
}